package it.lorix.trains.alg;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import it.lorix.trains.algorithm.CalculatePathMaxCostAlg;
import it.lorix.trains.constant.Message;

public class CalculatePathMaxCostAlgTest extends GraphBaseTest {

	private CalculatePathMaxCostAlg alg = null;

	@Before
	public void startup() {
		alg = new CalculatePathMaxCostAlg();
	}
	@Test
	public void getRouteWithMaxCost_inputGraphNull_ReturnGraphNodeEmptyMessage() {
		String result = alg.getRouteWithMaxCost(null, c, c, maxCost);
		Assert.assertEquals(Message.GRAPH_IS_EMPTY, result);
	}

	@Test
	public void getRouteWithMaxCost_inputGraphEmptyNodes_ReturnGraphNodeEmptyMessage() {
		String result = alg.getRouteWithMaxCost(graphNodesEmpty, c, c, maxCost);
		Assert.assertEquals(Message.GRAPH_IS_EMPTY, result);
	}

	@Test
	public void getRouteWithMaxCost_starNodeNull_ReturnStartEndNodeNullMessage() {
		String result = alg.getRouteWithMaxCost(graph, null, c, maxCost);
		Assert.assertEquals(Message.START_OR_END_NODE_NULL, result);
	}

	@Test
	public void getRouteWithMaxCost_endNodeNull_ReturnStartEndNodeNullMessage() {
		String result = alg.getRouteWithMaxCost(graph, c, null, maxCost);
		Assert.assertEquals(Message.START_OR_END_NODE_NULL, result);
	}

	@Test
	public void getRouteWithMaxCost_startEndNodeNull_ReturnStartEndNodeNullMessage() {
		String result = alg.getRouteWithMaxCost(graph, null, null, maxCost);
		Assert.assertEquals(Message.START_OR_END_NODE_NULL, result);
	}

	@Test
	public void getRouteWithMaxCost_costNegative_ReturnStopCostMustBePositiveMessage() {
		String result = alg.getRouteWithMaxCost(graph, c, c, -1);
		Assert.assertEquals(Message.NUM_STOP_COST_GREATER_ZERO, result);
	}

	@Test
	public void getRouteWithMaxCost_startNodeNotInGraph_ReturnStartEndNodeNotInGraphMessage() {
		String result = alg.getRouteWithMaxCost(graph, nodeNotInGraph, c, maxCost);
		Assert.assertEquals(Message.START_OR_END_NOT_IN_GRAPH, result);
	}

	@Test
	public void getRouteWithMaxCost_endNodeNotInGraph_ReturnStartEndNodeNotInGraphMessage() {
		String result = alg.getRouteWithMaxCost(graph, c, nodeNotInGraph, maxCost);
		Assert.assertEquals(Message.START_OR_END_NOT_IN_GRAPH, result);
	}

	@Test
	public void getRouteWithMaxCost_startEndNodeNotInGraph_ReturnStartEndNodeNotInGraphMessage() {
		String result = alg.getRouteWithMaxCost(graph, nodeNotInGraph, nodeNotInGraph, maxCost);
		Assert.assertEquals(Message.START_OR_END_NOT_IN_GRAPH, result);
	}

	@Test
	public void getRouteWithMaxCost_notExistingPath_Return0() {
		String result = alg.getRouteWithMaxCost(graph, d, a, maxCost);
		Assert.assertEquals("0", result);
	}

	@Test
	public void getRouteWithMaxCost_notExistingWithCost_Return0() {
		String result = alg.getRouteWithMaxCost(graph, c, c, 2);
		Assert.assertEquals("0", result);
	}

	@Test
	public void getRouteWithMaxCost_inputOk_Return2() {
		String result = alg.getRouteWithMaxCost(graph, c, c, maxCost);
		Assert.assertEquals("7", result);
	}
}
