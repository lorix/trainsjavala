package it.lorix.trains.alg;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.junit.Before;

import it.lorix.trains.dto.Edge;
import it.lorix.trains.dto.Graph;
import it.lorix.trains.dto.WeightedNode;

public class GraphBaseTest {

	protected Graph graph = null;
	protected Graph graphNodesNull = null;
	protected Graph graphNodesEmpty = null;

	protected LinkedList<String> route_1 = null;
	protected LinkedList<String> route_2 = null;
	protected LinkedList<String> route_3 = null;
	protected LinkedList<String> route_4 = null;
	protected LinkedList<String> route_5 = null;
	protected LinkedList<String> route_Self = null;
	protected LinkedList<String> route_with_null = null;
	protected LinkedList<String> route_not_exists_origin = null;
	protected LinkedList<String> route_empty = null;
	private List<String> routeList = null;

	protected String a;
	protected String b;
	protected String c;
	protected String d;
	protected String nodeNotInGraph;
	protected int maxStop;
	protected int exactStop;
	
	protected int maxCost;

	@Before
	public void startUp() {
		routeList = new ArrayList<String>();
		routeList.add("AB5");
		routeList.add("BC4");
		routeList.add("CD8");
		routeList.add("DC8");
		routeList.add("DE6");
		routeList.add("AD5");
		routeList.add("CE2");
		routeList.add("EB3");
		routeList.add("AE7");
		HashMap<String, WeightedNode> nodes = createNodes(routeList);
		graph = new Graph(nodes);
		graphNodesNull = new Graph(null);
		graphNodesEmpty = new Graph(new HashMap<String, WeightedNode>());
		// step 1-5
		route_1 = new LinkedList<String>(Arrays.asList("A", "B", "C"));
		route_2 = new LinkedList<String>(Arrays.asList("A", "D"));
		route_3 = new LinkedList<String>(Arrays.asList("A", "D", "C"));
		route_4 = new LinkedList<String>(Arrays.asList("A", "E", "B", "C", "D"));
		route_5 = new LinkedList<String>(Arrays.asList("A", "E", "D"));
		route_Self = new LinkedList<String>(Arrays.asList("D"));
		route_with_null = new LinkedList<String>(Arrays.asList("D", null));
		route_not_exists_origin = new LinkedList<String>(Arrays.asList("Z", "C"));
		route_empty = new LinkedList<String>();
		// step 6-7
		/*
		 * 6)The number of trips starting at C and ending at C with a maximum of
		 * 3 stops. In the sample data below, there are two such trips: C-D-C (2
		 * stops). and C-E-B-C (3 stops).
		 */
		/*
		 * 7)The number of trips starting at A and ending at C with exactly 4
		 * stops. In the sample data below, there are three such trips: A to C
		 * (via B,C,D); A to C (via D,C,D); and A to C (via D,E,B).
		 */
		a = "A";
		b = "B";
		c = "C";
		d = "D";
		nodeNotInGraph = "X";
		maxStop = 3;
		exactStop = 4;
		// step 8-9

		// step 10
		maxCost = 30;

	}

	private HashMap<String, WeightedNode> createNodes(List<String> routeList) {
		HashMap<String, WeightedNode> result = new HashMap<String, WeightedNode>();
		for (String route : routeList) {
			String start = String.valueOf(route.charAt(0));
			String end = String.valueOf(route.charAt(1));
			int distance = Integer.valueOf(route.substring(2)).intValue();
			WeightedNode node = result.get(start);
			// check if node already exists in result
			// if not, add
			if (node == null) {
				node = new WeightedNode(start);
				result.put(start, node);
			}
			// now add edge
			Edge edge = new Edge(end, distance);
			node.addEdge(edge);
		}
		return result;
	}
}
