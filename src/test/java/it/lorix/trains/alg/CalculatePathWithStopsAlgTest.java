package it.lorix.trains.alg;

import org.junit.Before;
import org.junit.Test;

import it.lorix.trains.algorithm.CalculatePathWithStopsAlg;
import it.lorix.trains.constant.Message;

import org.junit.Assert;

public class CalculatePathWithStopsAlgTest extends GraphBaseTest {

	private CalculatePathWithStopsAlg alg = null;

	@Before
	public void startup() {
		alg = new CalculatePathWithStopsAlg();
	}

	@Test
	public void getPathWithMaxStops_inputGraphNull_ReturnGraphNodeEmptyMessage() {
		String result = alg.getPathWithMaxStops(null, c, c, maxStop);
		Assert.assertEquals(Message.GRAPH_IS_EMPTY, result);
	}

	@Test
	public void getPathWithMaxStops_inputGraphEmptyNodes_ReturnGraphNodeEmptyMessage() {
		String result = alg.getPathWithMaxStops(graphNodesEmpty, c, c, maxStop);
		Assert.assertEquals(Message.GRAPH_IS_EMPTY, result);
	}

	@Test
	public void getPathWithMaxStops_starNodeNull_ReturnStartEndNodeNullMessage() {
		String result = alg.getPathWithMaxStops(graph, null, c, maxStop);
		Assert.assertEquals(Message.START_OR_END_NODE_NULL, result);
	}

	@Test
	public void getPathWithMaxStops_endNodeNull_ReturnStartEndNodeNullMessage() {
		String result = alg.getPathWithMaxStops(graph, c, null, maxStop);
		Assert.assertEquals(Message.START_OR_END_NODE_NULL, result);
	}

	@Test
	public void getPathWithMaxStops_startEndNodeNull_ReturnStartEndNodeNullMessage() {
		String result = alg.getPathWithMaxStops(graph, null, null, maxStop);
		Assert.assertEquals(Message.START_OR_END_NODE_NULL, result);
	}

	@Test
	public void getPathWithMaxStops_stopsNegative_ReturnStopCostMustBePositiveMessage() {
		String result = alg.getPathWithMaxStops(graph, c, c, -1);
		Assert.assertEquals(Message.NUM_STOP_COST_GREATER_ZERO, result);
	}

	@Test
	public void getPathWithMaxStops_startNodeNotInGraph_ReturnStartEndNodeNotInGraphMessage() {
		String result = alg.getPathWithMaxStops(graph, nodeNotInGraph, c, maxStop);
		Assert.assertEquals(Message.START_OR_END_NOT_IN_GRAPH, result);
	}

	@Test
	public void getPathWithMaxStops_endNodeNotInGraph_ReturnStartEndNodeNotInGraphMessage() {
		String result = alg.getPathWithMaxStops(graph, c, nodeNotInGraph, maxStop);
		Assert.assertEquals(Message.START_OR_END_NOT_IN_GRAPH, result);
	}

	@Test
	public void getPathWithMaxStops_startEndNodeNotInGraph_ReturnStartEndNodeNotInGraphMessage() {
		String result = alg.getPathWithMaxStops(graph, nodeNotInGraph, nodeNotInGraph, maxStop);
		Assert.assertEquals(Message.START_OR_END_NOT_IN_GRAPH, result);
	}

	@Test
	public void getPathWithMaxStops_notExistingPath_Return0() {
		String result = alg.getPathWithMaxStops(graph, d, a, maxStop);
		Assert.assertEquals("0", result);
	}

	@Test
	public void getPathWithMaxStops_notExistingWithStop_Return0() {
		String result = alg.getPathWithMaxStops(graph, a, c, 1);
		Assert.assertEquals("0", result);
	}

	@Test
	public void getPathWithMaxStops_inputOk_Return2() {
		String result = alg.getPathWithMaxStops(graph, c, c, maxStop);
		Assert.assertEquals("2", result);
	}
	
	//////////////
	//EXACT STOP//
	/////////////
	@Test
	public void getPathWithExactStops_inputGraphNull_ReturnGraphNodeEmptyMessage() {
		String result = alg.getPathWithExactStops(null, c, c, maxStop);
		Assert.assertEquals(Message.GRAPH_IS_EMPTY, result);
	}

	@Test
	public void getPathWithExactStops_inputGraphEmptyNodes_ReturnGraphNodeEmptyMessage() {
		String result = alg.getPathWithExactStops(graphNodesEmpty, c, c, maxStop);
		Assert.assertEquals(Message.GRAPH_IS_EMPTY, result);
	}

	@Test
	public void getPathWithExactStops_starNodeNull_ReturnStartEndNodeNullMessage() {
		String result = alg.getPathWithExactStops(graph, null, c, maxStop);
		Assert.assertEquals(Message.START_OR_END_NODE_NULL, result);
	}

	@Test
	public void getPathWithExactStops_endNodeNull_ReturnStartEndNodeNullMessage() {
		String result = alg.getPathWithExactStops(graph, c, null, maxStop);
		Assert.assertEquals(Message.START_OR_END_NODE_NULL, result);
	}

	@Test
	public void getPathWithExactStops_startEndNodeNull_ReturnStartEndNodeNullMessage() {
		String result = alg.getPathWithExactStops(graph, null, null, maxStop);
		Assert.assertEquals(Message.START_OR_END_NODE_NULL, result);
	}

	@Test
	public void getPathWithExactStops_stopsNegative_ReturnStopMustBePositiveMessage() {
		String result = alg.getPathWithExactStops(graph, c, c, -1);
		Assert.assertEquals(Message.NUM_STOP_COST_GREATER_ZERO, result);
	}

	@Test
	public void getPathWithExactStops_startNodeNotInGraph_ReturnStartEndNodeNotInGraphMessage() {
		String result = alg.getPathWithExactStops(graph, nodeNotInGraph, c, maxStop);
		Assert.assertEquals(Message.START_OR_END_NOT_IN_GRAPH, result);
	}

	@Test
	public void getPathWithExactStops_endNodeNotInGraph_ReturnStartEndNodeNotInGraphMessage() {
		String result = alg.getPathWithExactStops(graph, c, nodeNotInGraph, maxStop);
		Assert.assertEquals(Message.START_OR_END_NOT_IN_GRAPH, result);
	}

	@Test
	public void getPathWithExactStops_startEndNodeNotInGraph_ReturnStartEndNodeNotInGraphMessage() {
		String result = alg.getPathWithExactStops(graph, nodeNotInGraph, nodeNotInGraph, maxStop);
		Assert.assertEquals(Message.START_OR_END_NOT_IN_GRAPH, result);
	}

	@Test
	public void getPathWithExactStops_notExistingPath_Return0() {
		String result = alg.getPathWithExactStops(graph, d, a, maxStop);
		Assert.assertEquals("0", result);
	}

	@Test
	public void getPathWithExactStops_notExistingWithStop_Return0() {
		String result = alg.getPathWithExactStops(graph, a, c, 1);
		Assert.assertEquals("0", result);
	}

	@Test
	public void getPathWithExactStops_inputOk_Return3() {
		String result = alg.getPathWithExactStops(graph, a, c, exactStop);
		Assert.assertEquals("3", result);
	}
}
