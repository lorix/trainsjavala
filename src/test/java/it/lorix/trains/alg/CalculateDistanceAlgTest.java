package it.lorix.trains.alg;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import it.lorix.trains.algorithm.CalculateDistanceAlg;
import it.lorix.trains.constant.Message;

public class CalculateDistanceAlgTest extends GraphBaseTest {

	private CalculateDistanceAlg alg = null;

	@Before
	public void startup() {
		alg = new CalculateDistanceAlg();
	}

	@Test
	public void getDistanceInRoute_inputGraphNull_ReturnInputIncompleteMessage() {
		String result = alg.getDistanceInRoute(null, route_1);
		Assert.assertEquals(Message.INPUT_INCOMPLETE, result);
	}

	@Test
	public void getDistanceInRoute_inputRouteNull_ReturnInputIncompleteMessage() {
		String result = alg.getDistanceInRoute(graph, null);
		Assert.assertEquals(Message.INPUT_INCOMPLETE, result);
	}

	@Test
	public void getDistanceInRoute_inputsGraphAndRouteNull_ReturnInputIncompleteMessage() {
		String result = alg.getDistanceInRoute(null, null);
		Assert.assertEquals(Message.INPUT_INCOMPLETE, result);
	}

	@Test
	public void getDistanceInRoute_inputGraphNodesIsNull_ReturnGraphNodeEmptyMessage() {
		String result = alg.getDistanceInRoute(graphNodesNull, route_1);
		Assert.assertEquals(Message.GRAPH_IS_EMPTY, result);
	}

	@Test
	public void getDistanceInRoute_inputGraphNodesIsEmpty_ReturnGraphNodeEmptyMessage() {
		String result = alg.getDistanceInRoute(graphNodesEmpty, route_1);
		Assert.assertEquals(Message.GRAPH_IS_EMPTY, result);
	}

	@Test
	public void getDistanceInRoute_inputRouteIsEmpty_ReturnRouteEmptyMessage() {
		String result = alg.getDistanceInRoute(graph, route_empty);
		Assert.assertEquals(Message.ROUTE_EMPTY, result);
	}

	@Test
	public void getDistanceInRoute_inputRouteWithNull_ReturnRouteWithNullMessage() {
		String result = alg.getDistanceInRoute(graph, route_with_null);
		Assert.assertEquals(Message.ROUTE_CONTAINS_NULL_NODE, result);
	}

	@Test
	public void getDistanceInRoute_inputRouteSelf_ReturnResult0() {
		String result = alg.getDistanceInRoute(graph, route_Self);
		Assert.assertEquals("0", result);
	}

	@Test
	public void getDistanceInRoute_inputRouteOk1_ReturnResult9() {
		String result = alg.getDistanceInRoute(graph, route_1);
		Assert.assertEquals("9", result);
	}

	@Test
	public void getDistanceInRoute_inputRouteOk2_ReturnResult5() {
		String result = alg.getDistanceInRoute(graph, route_2);
		Assert.assertEquals("5", result);
	}

	@Test
	public void getDistanceInRoute_inputRouteOk3_ReturnResult13() {
		String result = alg.getDistanceInRoute(graph, route_3);
		Assert.assertEquals("13", result);
	}

	@Test
	public void getDistanceInRoute_inputRouteOk4_ReturnResult22() {
		String result = alg.getDistanceInRoute(graph, route_4);
		Assert.assertEquals("22", result);
	}

	@Test
	public void getDistanceInRoute_inputRouteNotExist_ReturnNoSuchRouteMessage() {
		String result = alg.getDistanceInRoute(graph, route_5);
		Assert.assertEquals(Message.NO_SUCH_ROUTE, result);
	}
}
