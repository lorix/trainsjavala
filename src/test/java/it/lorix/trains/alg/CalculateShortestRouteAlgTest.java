package it.lorix.trains.alg;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import it.lorix.trains.algorithm.CalculateShortestRouteAlg;
import it.lorix.trains.constant.Message;

public class CalculateShortestRouteAlgTest extends GraphBaseTest {

	private CalculateShortestRouteAlg alg = null;

	@Before
	public void startup() {
		alg = new CalculateShortestRouteAlg();
	}
	
	@Test
	public void getShortestLength_inputGraphNull_ReturnGraphNodeEmptyMessage() {
		String result = alg.getShortestLength(null, c, c);
		Assert.assertEquals(Message.GRAPH_IS_EMPTY, result);
	}

	@Test
	public void getShortestLength_inputGraphEmptyNodes_ReturnGraphNodeEmptyMessage() {
		String result = alg.getShortestLength(graphNodesEmpty, c, c);
		Assert.assertEquals(Message.GRAPH_IS_EMPTY, result);
	}

	@Test
	public void getShortestLength_starNodeNull_ReturnStartEndNodeNullMessage() {
		String result = alg.getShortestLength(graph, null, c);
		Assert.assertEquals(Message.START_OR_END_NODE_NULL, result);
	}

	@Test
	public void getShortestLength_endNodeNull_ReturnStartEndNodeNullMessage() {
		String result = alg.getShortestLength(graph, c, null);
		Assert.assertEquals(Message.START_OR_END_NODE_NULL, result);
	}

	@Test
	public void getShortestLength_startEndNodeNull_ReturnStartEndNodeNullMessage() {
		String result = alg.getShortestLength(graph, null, null);
		Assert.assertEquals(Message.START_OR_END_NODE_NULL, result);
	}
	
	@Test
	public void getShortestLength_startNodeNotInGraph_ReturnStartEndNodeNotInGraphMessage() {
		String result = alg.getShortestLength(graph, nodeNotInGraph, c);
		Assert.assertEquals(Message.START_OR_END_NOT_IN_GRAPH, result);
	}

	@Test
	public void getShortestLength_endNodeNotInGraph_ReturnStartEndNodeNotInGraphMessage() {
		String result = alg.getShortestLength(graph, c, nodeNotInGraph);
		Assert.assertEquals(Message.START_OR_END_NOT_IN_GRAPH, result);
	}

	@Test
	public void getShortestLength_startEndNodeNotInGraph_ReturnStartEndNodeNotInGraphMessage() {
		String result = alg.getShortestLength(graph, nodeNotInGraph, nodeNotInGraph);
		Assert.assertEquals(Message.START_OR_END_NOT_IN_GRAPH, result);
	}
	
	@Test
	public void getShortestLength_notExistingPath_ReturnNoSuchRouteMessage() {
		String result = alg.getShortestLength(graph, d, a);
		Assert.assertEquals(Message.NO_SUCH_ROUTE, result);
	}
	
	@Test
	public void getShortestLength_PathOKAC_Return9() {
		String result = alg.getShortestLength(graph, a, c);
		Assert.assertEquals("9", result);
	}
	@Test
	public void getShortestLength_PathOKCC_Return9() {
		String result = alg.getShortestLength(graph, c, c);
		Assert.assertEquals("9", result);
	}

}
