package it.lorix.trains.util;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Logger;

import it.lorix.trains.exception.TrainException;

public class FileReaderUtils {

	private static final Logger logger = Logger.getLogger(FileReaderUtils.class.getName());

	/**
	 * Read from file and return the content as String, leaving only char A-Z,
	 * digit and comma
	 * 
	 * @param filePath
	 *            the path to the file
	 * @return the content as String if not blank (null or empty), null
	 *         otherwise
	 */
	public String readFromFile(String filePath) {
		if (StringUtils.isBlank(filePath)) {
			return null;
		}
		Path file = null;
		try {
			file = Paths.get(filePath);
		} catch (InvalidPathException e) {
			logger.severe(String.format("Error: invalid path %s", filePath));
			throw new TrainException(e);
		}
		String fileString = null;
		try {
			fileString = new String(Files.readAllBytes(file), StandardCharsets.UTF_8);
		} catch (IOException e) {
			logger.severe(String.format("Error while reading file: %s. %s", fileString, e));
			throw new TrainException(e);
		}
		if (StringUtils.isBlank(fileString)) {
			return null;
		}
		logger.info(String.format("File is: %s", fileString));
		// remove all char except A-Z, digits and comma
		String cleanInput = fileString.replaceAll("[^A-Z0-9,]", "").trim();
		logger.info(String.format("Clean input is: \"%s\"", cleanInput));
		if (StringUtils.isBlank(cleanInput)) {
			return null;
		}
		return cleanInput;
	}
}
