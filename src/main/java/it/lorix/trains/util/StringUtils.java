package it.lorix.trains.util;

import java.util.logging.Logger;

public class StringUtils {
	private static final Logger logger = Logger.getLogger(StringUtils.class.getName());

	public static boolean isBlank(final CharSequence cs) {
		int strLen;
		if (cs == null || (strLen = cs.length()) == 0) {
			return true;
		}
		for (int i = 0; i < strLen; i++) {
			if (Character.isWhitespace(cs.charAt(i)) == false) {
				return false;
			}
		}
		return true;
	}
}
