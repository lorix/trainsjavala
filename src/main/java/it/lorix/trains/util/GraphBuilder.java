package it.lorix.trains.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import it.lorix.trains.constant.Message;
import it.lorix.trains.dto.Edge;
import it.lorix.trains.dto.Graph;
import it.lorix.trains.dto.WeightedNode;
import it.lorix.trains.exception.TrainException;

public class GraphBuilder {

	private static final Logger logger = Logger.getLogger(GraphBuilder.class.getName());

	private static final String ROUTE_PATTERN = "^[A-Z]{2}[0-9]+$";

	/**
	 * Given a String that contains a set of routes , return the directed
	 * weighted corresponding Graph instance.
	 * 
	 * @param sGraph
	 * @return the graph or null if something went wrong.
	 */
	public Graph buildGraph(String sGraph) {
		if (StringUtils.isBlank(sGraph)) {
			logger.severe(String.format(Message.INPUT_BLANK));
			throw new TrainException(Message.INPUT_BLANK);
		}
		List<String> routList = new ArrayList<String>();
		String[] routes = sGraph.split(",");
		if (routes == null || routes.length == 0) {
			logger.severe(String.format(Message.NO_ROUTES_FOUND));
			throw new TrainException(Message.NO_ROUTES_FOUND);
		}
		for (int i = 0; i < routes.length; i++) {
			routList.add(routes[i]);
		}
		if (routList.isEmpty()) {
			logger.severe(String.format(Message.NO_ROUTES_FOUND));
			throw new TrainException(Message.NO_ROUTES_FOUND);
		}
		HashMap<String, WeightedNode> nodes = createNodes(routList);
		if (nodes != null && !nodes.isEmpty()) {
			return new Graph(nodes);
		} else {
			return null;
		}
	}

	private HashMap<String, WeightedNode> createNodes(List<String> routeList) {
		HashMap<String, WeightedNode> result = new HashMap<String, WeightedNode>();
		for (String route : routeList) {
			if (isRoutePatternOk(route)) {
				logger.info(String.format("Found route: %s", route));
				String start = String.valueOf(route.charAt(0));
				String end = String.valueOf(route.charAt(1));
				int distance = Integer.valueOf(route.substring(2)).intValue();
				WeightedNode node = result.get(start);
				// check if node already exists in result
				// if not, add
				if (node == null) {
					node = new WeightedNode(start);
					result.put(start, node);
				}
				// now add edge
				Edge edge = new Edge(end, distance);
				node.addEdge(edge);
			}
		}
		return result;
	}

	public boolean isRoutePatternOk(String route) {
		return !StringUtils.isBlank(route) && route.matches(ROUTE_PATTERN);
	}
}
