package it.lorix.trains.algorithm;

import java.util.Collection;
import java.util.LinkedList;

import it.lorix.trains.constant.Message;
import it.lorix.trains.dto.Edge;
import it.lorix.trains.dto.Graph;
import it.lorix.trains.dto.Path;
import it.lorix.trains.dto.WeightedNode;
import it.lorix.trains.util.StringUtils;

public class CalculateShortestRouteAlg extends CalculatePathGenericAlg {

	private int minDistance;
	private Path shortestPath;

	public String getShortestLength(Graph graph, String start, String end) {
		minDistance = Integer.MAX_VALUE;
		String validation = validate(graph, start, end);
		if (!StringUtils.isBlank(validation)) {
			return validation;
		}
		LinkedList<Path> queue = new LinkedList<>();

		Path pStart = new Path();
		WeightedNode startNode = graph.getNode(start);
		pStart.addNode(startNode);

		queue.add(pStart);

		while (!queue.isEmpty()) {
			Path path = queue.poll();
			if (path != null) {
				WeightedNode lastNode = path.getLastNode();
				WeightedNode endNode = graph.getNode(end);
				if (lastNode.equals(endNode) && path.getDistance() > 0) {
					if (path.getDistance() < minDistance) {
						minDistance = path.getDistance();
						shortestPath = path;
					}
				} else {
					if (lastNode.getEdges() != null && !lastNode.getEdges().isEmpty()) {
						Collection<Edge> destinations = lastNode.getEdges();
						for (Edge e : destinations) {
							WeightedNode destinationNode = graph.getNode(e.getEnd());
							Path newPath = path.addEdge(e, destinationNode);
							if (newPath.getDistance() < minDistance && newPath.getNodeOccurrence(destinationNode.getName()) <=2) {
								queue.add(newPath);
							}
						}
					}
				}
			}

		}

		if (minDistance == Integer.MAX_VALUE) {
			return Message.NO_SUCH_ROUTE;
		}
		return String.valueOf(minDistance);
	}

	public Path getShortestPath() {
		return shortestPath;
	}

}
