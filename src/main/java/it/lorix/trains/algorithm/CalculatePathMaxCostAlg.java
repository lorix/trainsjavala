package it.lorix.trains.algorithm;

import java.util.ArrayList;
import java.util.Collection;

import it.lorix.trains.constant.Message;
import it.lorix.trains.dto.Edge;
import it.lorix.trains.dto.Graph;
import it.lorix.trains.dto.Path;
import it.lorix.trains.dto.WeightedNode;
import it.lorix.trains.util.StringUtils;

public class CalculatePathMaxCostAlg extends CalculatePathGenericAlg{

	public String getRouteWithMaxCost(Graph graph, String start, String end, int cost) {
		String validation = validate(graph, start, end, cost);
		if (!StringUtils.isBlank(validation)) {
			return validation;
		}
		Path path = new Path();
		WeightedNode startNode = graph.getNode(start);
		path.addNode(startNode);
		ArrayList<Path> list = getRouteWithMaxCost(graph, path, end, cost);
		if (list != null) {
			return String.valueOf(list.size());
		} else {
			return Message.CANNOT_FIND_PATH;
		}
	}

	private ArrayList<Path> getRouteWithMaxCost(Graph graph, Path path, String end, int cost) {
		ArrayList<Path> list = new ArrayList<Path>();
		WeightedNode node = path.getLastNode();
		WeightedNode endNode = graph.getNode(end);
		if (node.equals(endNode) && path.getDistance() < cost && path.getDistance() > 0) {
			list.add(path);
		}
		if (node.getEdges() != null && !node.getEdges().isEmpty()) {
			Collection<Edge> destinations = node.getEdges();
			for (Edge edge : destinations) {
				WeightedNode destinationNode = graph.getNode(edge.getEnd());
				Path next = path.addEdge(edge, destinationNode);
				if (next.getDistance() < cost) {
					ArrayList<Path> pathList = getRouteWithMaxCost(graph, next, end, cost);
					list.addAll(pathList);
				}
			}
		}
		return list;
	}
}
