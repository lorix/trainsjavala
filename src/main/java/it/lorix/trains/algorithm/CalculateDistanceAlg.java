package it.lorix.trains.algorithm;

import java.util.ArrayList;
import java.util.LinkedList;

import it.lorix.trains.constant.Message;
import it.lorix.trains.dto.Edge;
import it.lorix.trains.dto.Graph;
import it.lorix.trains.dto.WeightedNode;

public class CalculateDistanceAlg {

	/**
	 * Calculate the route distance in the graph. The route is assumed to be
	 * ordered.First is origin, last is destination
	 * 
	 * @param graph
	 * @param route
	 * @return
	 */

	public String getDistanceInRoute(Graph graph, LinkedList<String> route) {
		if (graph == null || route == null) {
			return Message.INPUT_INCOMPLETE;
		}
		if (graph.getNodes() == null || graph.getNodes().isEmpty()) {
			return Message.GRAPH_IS_EMPTY;
		}
		if (route.isEmpty()) {
			return Message.ROUTE_EMPTY;
		}
		if (route.contains(null)) {
			return Message.ROUTE_CONTAINS_NULL_NODE;
		}
		String originName = route.removeFirst();
		WeightedNode origin = graph.getNodes().get(originName);
		if (origin == null) {
			return Message.NO_SUCH_ROUTE;
		}
		int distance = 0;
		origin.setVisited(true);

		LinkedList<WeightedNode> queue = new LinkedList<WeightedNode>();
		queue.add(origin);

		while (!queue.isEmpty() && !route.isEmpty()) {
			// get node of graph
			WeightedNode node = queue.removeFirst();
			node.setVisited(true);
			// get node in route, we are searching for it in node's adjacent
			// vertex
			String next = route.removeFirst();

			ArrayList<Edge> edges = (ArrayList<Edge>) node.getEdges();
			if (edges != null && !edges.isEmpty()) {
				// search for next node in route in neighbors current node
				int index = edges.indexOf(new Edge(next));
				if (index < 0) {
					return Message.NO_SUCH_ROUTE;
				}
				int indexNeighbor = edges.indexOf(new Edge(next));
				Edge neighbor = edges.get(indexNeighbor);
				// get node from graph
				WeightedNode neighborNode = graph.getNodes().get(neighbor.getEnd());
				// assuming node in wellformed
				queue.add(neighborNode);
				distance += neighbor.getWeight();
			}
		}

		return String.valueOf(distance);
	}

}
