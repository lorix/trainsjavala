package it.lorix.trains.algorithm;

import it.lorix.trains.constant.Message;
import it.lorix.trains.dto.Graph;
import it.lorix.trains.util.StringUtils;

public class CalculatePathGenericAlg {

	protected String validate(Graph graph, String start, String end, int max) {
		String validation = validate(graph, start, end);
		if(!StringUtils.isBlank(validation)){
			return validation;
		}
		if (max <= 0) {
			return Message.NUM_STOP_COST_GREATER_ZERO;
		}
		return null;
	}

	protected String validate(Graph graph, String start, String end) {
		if (graph == null) {
			return Message.GRAPH_IS_EMPTY;
		}
		if (graph.getNodes() == null || graph.getNodes().isEmpty()) {
			return Message.GRAPH_IS_EMPTY;
		}
		if (StringUtils.isBlank(start) || StringUtils.isBlank(end)) {
			return Message.START_OR_END_NODE_NULL;
		}
		if (graph.getNode(start) == null || graph.getNode(end) == null) {
			return Message.START_OR_END_NOT_IN_GRAPH;
		}
		
		return null;
	}
}
