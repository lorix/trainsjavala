package it.lorix.trains.algorithm;

import java.util.ArrayList;
import java.util.Collection;

import it.lorix.trains.constant.Message;
import it.lorix.trains.dto.Edge;
import it.lorix.trains.dto.Graph;
import it.lorix.trains.dto.Path;
import it.lorix.trains.dto.WeightedNode;
import it.lorix.trains.util.StringUtils;

public class CalculatePathWithStopsAlg extends CalculatePathGenericAlg{

	public String getPathWithExactStops(Graph graph, String start, String end, int max) {

		String validation = validate(graph, start, end, max);
		if (!StringUtils.isBlank(validation)) {
			return validation;
		}
		Path path = new Path();
		WeightedNode startNode = graph.getNode(start);
		path.addNode(startNode);

		ArrayList<Path> list = getPathWithExactStops(graph, path, end, max);
		if (list != null) {
			return String.valueOf(list.size());
		} else {
			return Message.CANNOT_FIND_PATH;
		}

	}

	public String getPathWithMaxStops(Graph graph, String start, String end, int max) {

		String validation = validate(graph, start, end, max);
		if (!StringUtils.isBlank(validation)) {
			return validation;
		}
		Path path = new Path();
		WeightedNode startNode = graph.getNode(start);
		path.addNode(startNode);

		ArrayList<Path> list = getPathWithMaxStops(graph, path, end, max);

		if (list != null) {
			return String.valueOf(list.size());
		} else {
			return Message.CANNOT_FIND_PATH;
		}
	}

	private ArrayList<Path> getPathWithExactStops(Graph graph, Path path, String end, int max) {

		ArrayList<Path> list = new ArrayList<>();
		WeightedNode node = path.getLastNode();
		int pathSize = path.getPath().size();
		WeightedNode endNode = graph.getNode(end);
		if (node.equals(endNode) && pathSize > 1 && pathSize == max + 1) {
			list.add(path);
		} else {
			if (node.getEdges() != null) {
				Collection<Edge> destinations = node.getEdges();
				for (Edge edge : destinations) {
					WeightedNode destinationNode = graph.getNode(edge.getEnd());
					Path newPath = path.addEdge(edge, destinationNode);
					if (newPath.getPath().size() <= max + 1) {
						ArrayList<Path> paths = getPathWithExactStops(graph, newPath, end, max);
						list.addAll(paths);
					}
				}
			}
		}
		return list;
	}

	private ArrayList<Path> getPathWithMaxStops(Graph graph, Path path, String end, int max) {

		ArrayList<Path> list = new ArrayList<>();
		WeightedNode node = path.getLastNode();
		int pathSize = path.getPath().size();
		WeightedNode endNode = graph.getNode(end);
		if (node.equals(endNode) && pathSize > 1 && pathSize <= max + 1) {
			list.add(path);
		}
		if (node.getEdges() != null) {
			Collection<Edge> destinations = node.getEdges();
			for (Edge edge : destinations) {
				WeightedNode destinationNode = graph.getNode(edge.getEnd());
				Path newPath = path.addEdge(edge, destinationNode);
				if (newPath.getPath().size() <= max + 1) {
					ArrayList<Path> paths = getPathWithMaxStops(graph, newPath, end, max);
					list.addAll(paths);
				}
			}
		}
		return list;
	}

	
}
