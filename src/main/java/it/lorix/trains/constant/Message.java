package it.lorix.trains.constant;

public class Message {

	public static final String INPUT_BLANK = "Input string is blank.";
	public static final String NO_ROUTES_FOUND = "No routes found.";

	public static final String INPUT_INCOMPLETE = "Input incomplete.";
	public static final String NO_SUCH_ROUTE = "NO SUCH ROUTE";
	public static final String ROUTE_EMPTY = "Route is empty.";
	public static final String ROUTE_CONTAINS_NULL_NODE = "Route contains at least one null node.";
	public static final String GRAPH_IS_EMPTY = "Graph is empty.";
	
	
	public static final String START_OR_END_NODE_NULL = "Start and end node can't be null";
	public static final String NUM_STOP_COST_GREATER_ZERO = "The number of stops/cost must be graeater than 0";
	public static final String START_OR_END_NOT_IN_GRAPH = "Start or end node is not in graph";
	public static final String CANNOT_FIND_PATH = "Can't find path.";

	
	
	
}
