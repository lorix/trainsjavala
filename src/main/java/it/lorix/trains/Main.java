package it.lorix.trains;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import it.lorix.trains.algorithm.CalculateDistanceAlg;
import it.lorix.trains.algorithm.CalculatePathMaxCostAlg;
import it.lorix.trains.algorithm.CalculatePathWithStopsAlg;
import it.lorix.trains.algorithm.CalculateShortestRouteAlg;
import it.lorix.trains.dto.Graph;
import it.lorix.trains.dto.Path;
import it.lorix.trains.dto.WeightedNode;
import it.lorix.trains.exception.TrainException;
import it.lorix.trains.util.FileReaderUtils;
import it.lorix.trains.util.GraphBuilder;
import it.lorix.trains.util.StringUtils;

public class Main {

	private static final Logger logger = Logger.getLogger(Main.class.getName());

	public static void main(String[] args) {
		String fileArg = null;
		// default global log level: SEVERE
		setDebugLevel(Level.SEVERE);
		// check command line arguments.
		if (args.length == 0) {
			printHelpAndExit();
		}
		if (args.length > 2) {
			printHelpAndExit();
		}
		if (args.length == 2) {
			List<String> argList = new ArrayList<String>(Arrays.asList(args));
			int indexVerbose = argList.indexOf("-v");
			// must contains -v
			if (indexVerbose < 0) {
				printHelpAndExit();
			} else {
				// command line arg contains -v
				setDebugLevel(Level.INFO);
				argList.remove(indexVerbose);
				fileArg = argList.get(0);
			}
		}
		if (args.length == 1) {
			fileArg = args[0];
		}
		if (StringUtils.isBlank(fileArg)) {
			printHelpAndExit();
		}
		// READ CONTENT FROM FILE
		logger.info(String.format("Reading file  %s", fileArg));
		String fileContent = null;
		try {
			FileReaderUtils fileReader = new FileReaderUtils();
			fileContent = fileReader.readFromFile(fileArg);
			if (StringUtils.isBlank(fileContent)) {
				logger.severe(String.format("File %s is blank.", fileArg));
				System.exit(0);
			}
		} catch (TrainException e) {
			// already logged
			System.exit(0);
		} catch (Throwable e) {
			logger.severe(String.format("Unexpected error: %s", e));
			System.exit(0);
		}

		// BUILD GRAPH
		logger.info(String.format("Building graph from   \"%s\"", fileContent));
		Graph graph = null;
		try {
			GraphBuilder gBuilder = new GraphBuilder();
			graph = gBuilder.buildGraph(fileContent);
			if (graph == null) {
				logger.severe(String.format("Graph is null"));
				System.exit(0);
			}
			logger.info(String.format("Start algorithms on graph %s ", graph.toString()));
		} catch (TrainException e) {
			// already logged
			System.exit(0);
		} catch (Throwable e) {
			logger.severe(String.format("Unexpected error: %s", e));
			System.exit(0);
		}
		// calculate distance
		try {
			setDebugLevel(Level.INFO);
			LinkedList<String> route_1 = new LinkedList<String>(Arrays.asList("A", "B", "C"));
			LinkedList<String> route_2 = new LinkedList<String>(Arrays.asList("A", "D"));
			LinkedList<String> route_3 = new LinkedList<String>(Arrays.asList("A", "D", "C"));
			LinkedList<String> route_4 = new LinkedList<String>(Arrays.asList("A", "E", "B", "C", "D"));
			LinkedList<String> route_5 = new LinkedList<String>(Arrays.asList("A", "E", "D"));
			List<LinkedList<String>> routeList = new ArrayList<LinkedList<String>>();
			routeList.add(route_1);
			routeList.add(route_2);
			routeList.add(route_3);
			routeList.add(route_4);
			routeList.add(route_5);
			CalculateDistanceAlg alg = new CalculateDistanceAlg();
			for (int i = 0; i < routeList.size(); i++) {
				logger.info(String.format("Output #%d: %s", i + 1, alg.getDistanceInRoute(graph, routeList.get(i))));
			}

			String c = "C";

			CalculatePathWithStopsAlg algStop = new CalculatePathWithStopsAlg();
			String numRoutes = algStop.getPathWithMaxStops(graph, c, c, 3);
			logger.info(String.format("Output #6: %s", numRoutes));

			String a = "A";
			String numRoutes2 = algStop.getPathWithExactStops(graph, a, c, 4);
			logger.info(String.format("Output #7: %s", numRoutes2));
			
			CalculateShortestRouteAlg shortestAlg = new CalculateShortestRouteAlg();
			String numShortes = shortestAlg.getShortestLength(graph, a, c);
			logger.info(String.format("Output #8: %s", numShortes));
			String b = "B";
			numShortes = shortestAlg.getShortestLength(graph, b, b);
			logger.info(String.format("Output #9: %s", numShortes));

			CalculatePathMaxCostAlg algMaxCost = new CalculatePathMaxCostAlg();
			String numPath = algMaxCost.getRouteWithMaxCost(graph, c, c, 30);
			logger.info(String.format("Output #10: %s", numPath));

		} catch (TrainException e) {
			// already logged
			System.exit(0);
		} catch (Throwable e) {
			logger.severe(String.format("Unexpected error: %s", e));
			e.printStackTrace();
			System.exit(0);
		}

	}

	private static void setDebugLevel(Level newLvl) {
		Logger rootLogger = LogManager.getLogManager().getLogger("");
		Handler[] handlers = rootLogger.getHandlers();
		rootLogger.setLevel(newLvl);
		for (Handler h : handlers) {
			h.setLevel(newLvl);
		}
	}

	private static void printHelpAndExit() {
		System.out.println("USAGE:");
		System.out.println("\t mvn -q compile exec:java -Dexec.args=\"file  [-v]\"");
		System.out.println("ARGS:");
		System.out.println("\t file : path to file with input");
		System.out.println("\t -v : verbose output");
		System.exit(0);
	}
}
