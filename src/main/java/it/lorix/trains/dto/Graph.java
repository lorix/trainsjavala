package it.lorix.trains.dto;

import java.util.HashMap;

public class Graph {

	private HashMap<String, WeightedNode> nodes;

	public Graph(HashMap<String, WeightedNode> nodes) {
		this.nodes = nodes;
	}

	public HashMap<String, WeightedNode> getNodes() {
		return nodes;
	}

	public void setNodes(HashMap<String, WeightedNode> nodes) {
		this.nodes = nodes;
	}

	@Override
	public String toString() {
		return "Graph =" + nodes;
	}

	public WeightedNode getNode(String name) {
		if (this.nodes == null) {
			return null;
		} else {
			return nodes.get(name);
		}
	}

}
