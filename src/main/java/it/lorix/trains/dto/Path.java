package it.lorix.trains.dto;

import java.util.ArrayList;
import java.util.HashMap;

public class Path {
	private ArrayList<WeightedNode> path;
	private HashMap<String, Integer> occurrence;
	private int distance;

	public Path() {
		this.path = new ArrayList<WeightedNode>();
		this.occurrence = new HashMap<>();
	}

	public int getDistance() {
		return distance;
	}

	public void setDistance(int distance) {
		this.distance = distance;
	}

	public WeightedNode getLastNode() {
		if (path.size() == 0)
			return null;
		return path.get(path.size() - 1);
	}

	public void addNode(WeightedNode node) {
		this.path.add(node);
		incrementOccurrence(node.getName());
	}

	public Path addEdge(Edge e, WeightedNode destinationNode) {
		Path p = new Path();
		p.path = new ArrayList<WeightedNode>();
		p.path.addAll(this.path);
		p.occurrence = new HashMap<>(this.occurrence);
		p.addNode(destinationNode);
		p.distance = this.distance + e.getWeight();
		return p;
	}

	public ArrayList<WeightedNode> getPath() {
		return path;
	}
	
	public Integer getNodeOccurrence(String name) {
		Integer nodeOccurrence = this.occurrence.get(name);
		if (nodeOccurrence == null) nodeOccurrence = 0;
		return nodeOccurrence;
	}
	
	private void incrementOccurrence(String name) {
		Integer nodeOccurrence = 0;
		if (this.occurrence.containsKey(name)) {
			nodeOccurrence = this.occurrence.get(name);
		}
		this.occurrence.put(name, ++nodeOccurrence);
	}

	public void setPath(ArrayList<WeightedNode> path) {
		this.path = path;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (WeightedNode node : path) {
			sb.append(node.getName()).append(" ");
		}
		sb.append(" cost: ").append(distance);
		return sb.toString();
	}

}
