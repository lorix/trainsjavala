package it.lorix.trains.dto;

import java.util.ArrayList;
import java.util.List;

public class WeightedNode {
	private String name;
	private boolean visited;
	private List<Edge> edges;

	public WeightedNode(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isVisited() {
		return visited;
	}

	public void setVisited(boolean visited) {
		this.visited = visited;
	}

	public List<Edge> getEdges() {
		return edges;
	}

	public void setEdges(List<Edge> edges) {
		this.edges = edges;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WeightedNode other = (WeightedNode) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	public void addEdge(Edge edge) {
		if (this.edges == null) {
			this.edges = new ArrayList<Edge>();
		}
		this.edges.add(edge);
	}

	@Override
	public String toString() {
		return "{" + (name != null ? name + ", " : "") + (edges != null ? "edges=" + edges : "") + "}";
	}

}
