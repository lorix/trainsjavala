package it.lorix.trains.dto;

public class Edge {

	private String end;
	private int weight;

	public Edge() {
	}

	public Edge(String end, int weight) {
		this.end = end;
		this.weight = weight;
	}

	public Edge(String end) {
		this.end = end;
	}

	public String getEnd() {
		return end;
	}

	public void setEnd(String end) {
		this.end = end;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Edge other = (Edge) obj;
		if (end == null) {
			if (other.end != null)
				return false;
		} else if (!end.equals(other.end))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "(" + (end != null ? end + ", " : "") + weight + ")";
	}

}
