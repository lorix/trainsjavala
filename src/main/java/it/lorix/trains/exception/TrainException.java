package it.lorix.trains.exception;

public class TrainException extends RuntimeException {

	private static final long serialVersionUID = 8066342069437016111L;

	public TrainException() {
	}

	public TrainException(String message) {
		super(message);
	}

	public TrainException(Throwable cause) {
		super(cause);
	}

	public TrainException(String message, Throwable cause) {
		super(message, cause);
	}

	public TrainException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
