# README #

TrainsJavaLA

The project consists of a Main class which is the entry point for the application, the class with the implementation of the algorithms and other useful classes (DTO, Custom exception, Utils, FileReader, Graph builder). 
I divided the problems in 4 different types of algorithm.

I developed the application with two different approaches.

1. For the first algorithm I use a pure TDD methodology. I wrote all the test first and than I wrote the implementation.
2. For the rest I choose to implement first a working prototype version. Next I wrote all the unit test to ensure the robustness of the code.
 
I assume that a Graph is always well formed.

# SYSTEM REQUIREMENTS #

 * Java 7 or higher
 * Maven 3.2.5 or higher

# USAGE #

From the root dir of the project (the one which contains pom.xml) run the command on shell:

USAGE:

* mvn [-q] compile test exec:java -Dexec.args="file  [-v]"
	 
ARGS:

* file : path to file with input. Required.
* -v : verbose output [Optional]
* -q is Maven Option for quite mode [Optional]